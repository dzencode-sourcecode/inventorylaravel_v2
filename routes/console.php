<?php

use Illuminate\Foundation\Inspiring;
use Illuminate\Support\Facades\DB;
/*
|--------------------------------------------------------------------------
| Console Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of your Closure based console
| commands. Each Closure is bound to a command instance allowing a
| simple approach to interacting with each command's IO methods.
|
*/

Artisan::command('inspire', function () {
    $this->comment(Inspiring::quote());
})->describe('Display an inspiring quote');

Artisan::command('generateModels', function () {

    $tables = DB::select('SHOW TABLES');
    foreach($tables as $table)
    {
        $tableName = $table->Tables_in_inventory;
        $parts = explode( '_',$tableName);
        $newName = '';
        foreach ($parts as $part) {
            $newName .= ucfirst($part);
        }
        $disabled = ['Migrations','PasswordResets'];

        if($newName == 'Users') {
            $newName = 'User';
        }
        if (!in_array($newName, $disabled)) {
            $this->comment($newName);
            `php artisan krlove:generate:model $newName --table-name=$tableName`;
        }

    }
})->describe('Display an inspiring quote');
