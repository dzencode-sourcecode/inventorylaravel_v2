<?php

//Route::get('/', function () {
//    return view('welcome');
//});
//Route::get('/', function() {
//    die('asd');
//});

Route::get('{roomId?}', 'SpaController@index')->middleware('saas')->name('index');

Route::group(['prefix' => 'api/{roomId}'], function() {
    Route::delete('users/{user}/products/{product}', 'UserController@deleteProduct');
    Route::delete('users/{user}/groups/{group}', 'UserController@deleteGroup');
    Route::get('users/getUser','UserController@getUser')->name('user.getUser');


    Route::apiResource('orders',"OrderController");

    Route::get('/categories/getObjects',"CategoryController@getObjects");
    Route::apiResource('categories',"CategoryController");
    Route::apiResource('specifications',"SpecificationController");

    Route::get('products/free', 'ProductController@freeProducts');
    Route::apiResource('products',"ProductController");

    Route::get('groups/getGroupUsers/{group}', 'GroupController@getGroupUsers');
    Route::apiResource('groups',"GroupController");
    Route::apiResource('statuses',"StatusController");
    Route::apiResource('users',"UserController");
    Route::post('groups/{group}/addProduct', 'GroupController@addProduct');
    Route::delete('groups/{group}/products/{product}', 'GroupController@deleteProduct');
    Route::post('groups/{group}/addUser/{user}', 'GroupController@addUser');
    Route::post('products/{user}/addProduct/{product}', 'UserController@addProductToUser');
});
Route::get('/{roomId?}/{any?}', 'SpaController@index')->where('any', '.*');
