/**
 * Router configuration
 */

import Vue from 'vue'
import Router from 'vue-router'
import Orders from '@/components/orders//Orders'
import Groups from '@/components/groups/Groups'
import Products from '@/components/products/Products'
import Users from '@/components/users/Users'
import Settings from '@/components/settings/Settings'
import ProductAdd from '@/components/productAdd/ProductAdd'
import Error from '@/components/pages/Error'
import GroupAddProduct from '@/components/groups/GroupAddProduct'
import MyRooms from '@/components/myRooms/MyRooms'
// import App from '@/components/App.vue'
// import Room from '@/components/room/Room'

// async version
// const Orders = () => import('@/components/orders//Orders')
// const Groups = () => import('@/components/groups/Groups')
// const Products = () => import('@/components/products/Products')
// const Users = () => import('@/components/users/Users')
// const Settings = () => import('@/components/settings/Settings')
// const ProductAdd = () => import('@/components/productAdd/ProductAdd')
// const Error = () => import('@/components/pages/Error')
// const GroupAddProduct = () => import('@/components/groups/GroupAddProduct')

Vue.use(Router)

const withPrefix = (prefix, routes) =>
  routes.map((route) => {
    route.path = prefix + route.path
    return route
  })

const router = new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'myRooms',
      component: MyRooms
    },
    ...withPrefix('/:roomId', [
      {
        path: '/',
        name: 'orders',
        component: Orders
      },
      {
        path: '/groups',
        name: 'groups',
        component: Groups
      },
      {
        path: '/products',
        name: 'products',
        component: Products
      },
      {
        path: '/users',
        name: 'users',
        component: Users
      },
      {
        path: '/settings',
        name: 'settings',
        component: Settings
      },
      {
        path: '/error',
        component: Error
      },
      {
        path: '/orders/:orderId/products/add',
        props: true,
        name: 'orders-add',
        component: ProductAdd
      },

      {
        path: '/categories/add',
        props: true,
        name: 'categories-add',
        component: ProductAdd
      },
      {
        path: '/specifications/add',
        props: true,
        name: 'specifications-add',
        component: ProductAdd
      },
      {
        path: '/groups/:groupId/products/add',
        props: true,
        name: 'groups-add',
        component: GroupAddProduct
      }
    ]),
    {path: '*', redirect: '/error'}
  ]
})

// router.beforeEach = (to, from, next) => {
//   console.log('start')
//   if (to.name !== 'myRooms') {
//     let roomId = to.params.roomId
//     let flag = true
//     if (this.user && this.user.user_service_room) {
//       for (let usr of this.user.user_service_room) {
//         if (usr.service_room_id === roomId) flag = false
//       }
//     }
//     if (flag) {
//       this.$router.push({name: 'myRooms'})
//     }
//   }
//
//   console.log('end')
// }
export default router
