import api from '../api'
import axios from 'axios'
import router from '../router'
// GET orders
const roomId = () => {
  return router.currentRoute.params.roomId
}
export const getUser = ({ commit }, payload) => {
  const urlEnd = `/api/${roomId()}/users/getUser`
  const type = 'get'
  api.user(urlEnd, type, payload)
    .then((res) => {
      commit('setUser', res.data)
    })
    .catch((err) => {
      console.error(err)
    })
}

export const getOrders = ({ commit }, payload) => {
  const urlEnd = `/api/${roomId()}/orders`
  const type = 'get'
  api.requestOrders(urlEnd, type, payload)
    .then((res) => {
      commit('setOrders', res.data)
    })
    .catch((err) => {
      console.error(err)
    })
}

// GET statuses
export const getStatuses = ({ commit }, payload) => {
  axios.get(`/api/${roomId()}/statuses`, payload)
    .then((res) => {
      commit('setStatuses', res.data)
    })
    .catch((err) => {
      console.error(err)
    })
}

// GET currencies
export const getCurrencies = ({ commit }, payload) => {
  const urlEnd = `/api/${roomId()}/currencies`
  const type = 'post'
  api.requestCurrencies(urlEnd, type, payload)
    .then((res) => {
      commit('setCurrencies', res)
    })
    .catch((err) => {
      console.error(err)
    })
}

// POST date to server
export const saveOrder = ({ commit }, payload) => {
  const urlEnd = `/api/${roomId()}/orders`
  const type = 'post'
  api.requestToServer(urlEnd, type, payload)
    .then((res) => {
      console.log('res', res)
      commit('addOrder', res.data)
    })
    .catch((err) => {
      console.error(err)
    })
}

// DELETE order by id
export const deleteOrder = ({ commit }, payload) => {
  axios.delete(`/api/${roomId()}/orders/${payload.id}`, payload)
    .then((res) => {
      commit('deleteOrder', payload)
    })
    .catch((err) => {
      console.error(err)
    })
}

export const deleteGroup = ({ commit }, group) => {
  axios.delete(`/api/${roomId()}/groups/${group.id}`, group)
    .then((res) => {
      commit('deleteGroup', group)
    })
    .catch((err) => {
      console.error(err)
    })
}
// DELETE order by id
export const deleteUserGroup = ({ commit }, payload) => {
  console.log('betogre', payload)
  axios.delete(`/api/${roomId()}/users/${payload.user.id}/groups/${payload.group.id}`, payload)
    .then((res) => {
      commit('deleteUserGroup', payload)
      getGroupUsers({commit}, payload.group.id)
    })
    .catch((err) => {
      console.error(err)
    })
}

export const deleteProductFromCategory = ({ commit }, product) => {
  axios.delete(`/api/${roomId()}/products/${product.id}`, product)
    .then((res) => {
      commit('deleteProductFromCategory', product)
    })
    .catch((err) => {
      console.error(err)
    })
}
export const deleteProductFromUser = ({ commit }, payload) => {
  axios.delete(`/api/${roomId()}/users/${payload.user.id}/products/${payload.product.id}`, payload)
    .then((res) => {
      commit('deleteProductFromUser', payload)
    })
    .catch((err) => {
      console.error(err)
    })
}

export const deleteUser = ({ commit }, user) => {
  axios.delete(`/api/${roomId()}/users/${user.id}`, user)
    .then((res) => {
      commit('deleteUser', user)
    })
    .catch((err) => {
      console.error(err)
    })
}

export const deleteOrderProduct = ({ commit }, payload) => {
  axios.delete(`/api/${roomId()}/products/${payload.product.id}`, payload)
    .then((res) => {
      commit('deleteOrderProduct', payload)
    })
    .catch((err) => {
      console.error(err)
    })
}
export const deleteGroupProduct = ({ commit }, payload) => {
  axios.delete(`/api/${roomId()}/groups/${payload.group.id}/products/${payload.product.id}`, payload)
    .then((res) => {
      commit('deleteGroupProduct', payload)
    })
    .catch((err) => {
      console.error(err)
    })
}

export const getUsers = ({ commit }, payload) => {
  axios.get(`/api/${roomId()}/users`, payload)
    .then((res) => {
      commit('setUsers', res.data)
    })
    .catch((err) => {
      console.error(err)
    })
}

export const getGroups = ({ commit }, payload) => {
  axios.get(`/api/${roomId()}/groups`, payload)
    .then((res) => {
      commit('setGroups', res.data)
    })
    .catch((err) => {
      console.error(err)
    })
}

export const getProducts = ({ commit }, payload) => {
  axios.get(`/api/${roomId()}/products`, payload)
    .then((res) => {
      commit('setProducts', res.data)
    })
    .catch((err) => {
      console.error(err)
    })
}

export const getSpecifications = ({ commit }, payload) => {
  console.log('spec payload', payload)
  axios.get(`/api/${roomId()}/specifications`, payload)
    .then((res) => {
      commit('setSpecifications', res.data)
    })
    .catch((err) => {
      console.error(err)
    })
}

export const getCategories = ({ commit }, payload) => {
  axios.get(`/api/${roomId()}/categories`, payload)
    .then((res) => {
      commit('setCategories', res.data)
    })
    .catch((err) => {
      console.error(err)
    })
}

export const addCategory = ({ commit }, payload) => {
  console.log(payload)
  axios.post(`/api/${roomId()}/categories`, payload)
    .then((res) => {
      commit('addCategory', res.data)
      getCategories({ commit })
    })
    .catch((err) => {
      console.error(err)
    })
}
export const addSpec = ({ commit }, payload) => {
  console.log('spec payload', payload)
  axios.post(`/api/${roomId()}/specifications`, payload)
    .then((res) => {
      commit('addSpec', res.data)
      getSpecifications({ commit })
      getCategories({ commit })
      getObjCategories({commit})
    })
    .catch((err) => {
      console.error(err)
    })
}

export const addProduct = ({ commit }, payload) => {
  console.log('products payload', payload)
  axios.post(`/api/${roomId()}/products`, payload)
    .then((res) => {
      commit('addProduct', res.data)
      router.push({name: 'orders'})
    })
    .catch((err) => {
      console.error(err)
    })
}

export const addUserToGroup = ({ commit }, payload) => {
  console.log('products payload', payload)
  axios.post(`/api/${roomId()}/groups/${payload.group.id}/addUser/${payload.user.id}`, payload)
    .then((res) => {
      commit('addUserToGroup', payload)
      commit('addGroupToUser', payload)
    })
    .catch((err) => {
      console.error(err)
    })
}

export const updateProduct = ({ commit }, payload) => {
  console.log('products update payload', payload)
  axios.put(`/api/${roomId()}/products/` + payload.id, payload)
    .then((res) => {
      commit('updateProduct', res.data)
      getObjCategories({ commit })
      getSpecifications({ commit })
      getProducts({ commit })
    })
    .catch((err) => {
      console.error(err)
    })
}

export const updateCategory = ({ commit }, category) => {
  axios.put(`/api/${roomId()}/categories/` + category.id, category)
    .then((res) => {
      getObjCategories({ commit })
      getSpecifications({ commit })
      getProducts({ commit })
      getCategories({ commit })
    })
    .catch((err) => {
      console.error(err)
    })
}
export const updateSpec = ({ commit }, payload) => {
  axios.put(`/api/${roomId()}/specifications/${payload.selectedSpecObj.id}`, payload)
    .then((res) => {
      getObjCategories({ commit })
      getSpecifications({ commit })
      getProducts({ commit })
      getCategories({ commit })
      getObjCategories({commit})
    })
    .catch((err) => {
      console.error(err)
    })
}

export const setCurrentOrder = ({commit}, payload) => {
  commit('setCurrentOrder', payload)
  console.log('current order has been set')
}

export async function getOrder ({commit}, orderId) {
  return axios.get(`/api/${roomId()}/orders/${orderId}`)
}

export async function getGroup ({commit}, groupId) {
  return axios.get(`/api/${roomId()}/groups/${groupId}`)
}

export const addGroup = ({ commit }, payload) => {
  console.log('add group', payload)
  axios.post(`/api/${roomId()}/groups`, payload)
    .then((res) => {
      commit('addGroup', res.data)
    })
    .catch((err) => {
      console.error(err)
    })
}

// export  function getFreeProducts ({commit}, payload) {
//   return axios.get(`/products/free`)
// }

export const getFreeProducts = ({ commit }, payload) => {
  axios.get(`/api/${roomId()}/products/free`)
    .then((res) => {
      commit('setFreeProducts', res.data)
      console.log('resp data', res.data)
    })
    .catch((err) => {
      console.error(err)
    })
}

export const addGroupProduct = ({ commit }, payload) => {
  console.log('add group product', payload)
  axios.post(`/api/${roomId()}/groups/${payload.group.id}/addProduct`, payload)
    .then((res) => {
      commit('addGroupProduct', {group: payload.group, product: res.data})
      getFreeProducts({ commit })
    })
    .catch((err) => {
      console.error(err)
    })
}

export const addProductToUser = ({ commit }, payload) => {
  console.log('add user product action', payload)
  axios.post(`/api/${roomId()}/products/${payload.user.id}/addProduct/${payload.product.id}`, payload)
    .then((res) => {
      commit('addProductToUser', payload)
      getFreeProducts({ commit })
    })
    .catch((err) => {
      console.error(err)
    })
}

export const addStatus = ({ commit }, payload) => {
  console.log('add user product action', payload)
  axios.post(`/api/${roomId()}/statuses`, payload)
    .then((res) => {
      commit('addStatus', res.data)
      getFreeProducts({ commit })
    })
    .catch((err) => {
      console.error(err)
    })
}

export const getObjCategories = ({ commit }, payload) => {
  axios.get(`/api/${roomId()}/categories/getObjects`)
    .then((res) => {
      commit('setObjCategories', res.data)
      console.log('resp data', res.data)
    })
    .catch((err) => {
      console.error(err)
    })
}

export const getGroupUsers = ({ commit }, groupId) => {
  axios.get(`/api/${roomId()}/groups/getGroupUsers/${groupId}`)
    .then((res) => {
      commit('setGroupUsers', res.data)
    })
    .catch((err) => {
      console.error(err)
    })
}
