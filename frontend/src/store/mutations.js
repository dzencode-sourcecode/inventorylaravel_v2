// Order mutators
export const setUser = (state, user) => {
  user.exipred = Date.now() + 1 * 60000
  state.user = user
  localStorage.setItem('user', JSON.stringify(user))
}

export const setOrders = (state, orders) => { state.orders = orders }
export const addOrder = (state, newOrder) => { state.orders.push(newOrder) }
export const deleteOrderById = (state, id) => { state.orders = state.orders.filter(order => order.id === id) }
export const deleteOrder = (state, order) => { state.orders = state.orders.filter(order1 => order1.id !== order.id) }
export const deleteGroup = (state, group) => { state.groups = state.groups.filter(group1 => group1.id !== group.id) }
export const deleteUser = (state, user) => { state.users = state.users.filter(user1 => user1.id !== user.id) }
export const deleteProductFromCategory = (state, product) => {
  for (let cat of state.objCategories) {
    for (let spec of cat.specs) {
      spec.products = spec.products.filter(prod1 => prod1.id !== product.id)
    }
  }
}
export const deleteProductFromUser = (state, payload) => {
  payload.user.products = payload.user.products.filter(prod => prod.id !== payload.product.id)
}
export const deleteOrderProduct = (state, payload) => {
  payload.order.products = payload.order.products.filter(prod => prod.id !== payload.product.id)
}
export const deleteGroupProduct = (state, payload) => {
  payload.group.products = payload.group.products.filter(prod => prod.id !== payload.product.id)
}
export const deleteUserGroup = (state, payload) => {
  payload.group.users = payload.group.users.filter(user => user.id !== payload.user.id)
  payload.user.groups = payload.user.groups.filter(group => group.id !== payload.group.id)
}
export const addSpec = (state, spec) => { state.specifications.push(spec) }
// Product mutators
export const setProducts = (state, products) => { state.products = products }
// Users mutators
export const setUsers = (state, users) => { state.users = users }
// Statuses mutators
export const setStatuses = (state, statuses) => { state.statuses = statuses }
export const addStatus = (state, status) => { state.statuses.push(status) }
// Currencies mutators
export const setCurrencies = (state, currencies) => { state.currencies = currencies }
// Groups mutators
export const setGroups = (state, groups) => { state.groups = groups }
export const addGroup = (state, group) => { state.groups.push(group) }
// Categories mutators
export const setCategories = (state, categories) => { state.categories = categories }
export const addCategory = (state, category) => { state.categories.push(category) }
// Specifications mutators
export const setSpecifications = (state, specifications) => { state.specifications = specifications }

export const addProduct = (state, product) => { state.products.push(product) }
export const updateProduct = (state, product) => {
  state.products = state.products.filter(product1 => product1.id !== product.id)
  console.log(state.products)

  state.products.push(product)
  console.log(state.products)
}

export const setCurrentOrder = (state, order) => { state.currentOrder = order }
export const addGroupProduct = (state, payload) => {
  payload.group.products.push(payload.product)
}
export const setFreeProducts = (state, freeProducts) => { state.freeProducts = freeProducts }
export const setObjCategories = (state, objCategories) => { state.objCategories = objCategories }
export const setCurrentCategory = (state, currentCategory) => { state.currentCategory = currentCategory }
export const setGroupUsers = (state, groupUsers) => { state.groupUsers = groupUsers }
export const addUserToGroup = (state, groupAndUser) => {
  state.groupUsers.push(groupAndUser.user)
}
export const addGroupToUser = (state, groupAndUser) => {
  groupAndUser.user.groups.push(groupAndUser.group)
}
export const addProductToUser = (state, productAndUser) => {
  productAndUser.user.products.push(productAndUser.product)
  console.log(productAndUser.user.products)
}
