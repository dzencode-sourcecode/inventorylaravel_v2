/**
 * Mixins for forms
 */

export default {
  data () {
    return {
      activeFormName: 'category'
    }
  },
  methods: {
    openNewForm (form) {
      this.activeFormName = form
    }
  }
}
