<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property int $order_id
 * @property int $currency_id
 * @property int $spec_id
 * @property string $sn
 * @property int $price
 * @property boolean $is_new
 * @property boolean $is_guarantee
 * @property string $guarantee_start
 * @property string $guarantee_stop
 * @property Currency $currency
 * @property Order $order
 * @property Spec $spec
 * @property Group[] $groups
 * @property User[] $users
 */
class Product extends Model
{

    public $timestamps = false;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'product';

    /**
     * @var array
     */
    protected $fillable = ['order_id', 'currency_id', 'spec_id', 'sn', 'price', 'is_new', 'is_guarantee', 'guarantee_start', 'guarantee_stop', 'room_id'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function currency()
    {
        return $this->belongsTo('App\Currency');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function order()
    {
        return $this->belongsTo('App\Order');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function spec()
    {
        return $this->belongsTo('App\Spec');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function groups()
    {
        return $this->belongsToMany('App\Group', 'product_has_group');
    }

    public function status()
    {
        return $this->belongsTo('App\Status', 'status_id');
    }


    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function users()
    {
        return $this->belongsToMany('App\User', 'user_has_item', 'item_id');
    }
}
