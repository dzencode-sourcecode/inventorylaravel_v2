<?php


namespace App\Facades;


use Illuminate\Support\Facades\Facade;

class Saas
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'saas';
    }

    public static function getRoles()
    {
        $roomId = request()->roomId;
        if(!$roomId) return [];
        $user = session('saas.user');
        $rooms = array_filter($user['user_service_rooms'], function ($usr) use ($roomId) {
            return $usr['service_room_id']== $roomId;
        });
        $room = array_values($rooms)[0];

        return $room ? $room['roles'] : [];
    }


    public static function getStrRoles()
    {
        $arr = self::getRoles();
        return array_map(function ($role) {
            return $role['name'];
        },$arr);

    }

    public static function hasRole($str)
    {
        return in_array($str, self::getStrRoles());
    }

}
