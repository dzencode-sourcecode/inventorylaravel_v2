<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $group_id
 * @property int $user_id
 * @property Group $group
 * @property User $user
 */
class GroupUser extends Model
{
    public $timestamps = false;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'group_user';

    /**
     * @var array
     */
    protected $fillable = ['group_id', 'user_id'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function group()
    {
        return $this->belongsTo('App\Group');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
