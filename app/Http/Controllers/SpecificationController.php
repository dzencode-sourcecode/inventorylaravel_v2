<?php

namespace App\Http\Controllers;

use App\Category;
use App\Spec;
use Illuminate\Http\Request;

class SpecificationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        return Spec::where('room_id', $request->roomId)->with(
            'products.spec.category',
            'products.order',
            'products.status',
            'products.groups',
            'products.currency'
            )->get();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $roomId)
    {
        $this->authorize('create', Spec::class);

        $category = Category::where('name',  $request->selectedCategory['name'] ?? $request->selectedCategory)->where('room_id', $roomId)->first();
        if(!$category) {
            return 'Category not found';
        }

        $spec = new Spec();
        $spec->title = $request->title;
        $spec->category()->associate($category);


        $allowableTypes = ['png', 'jpg', 'jpeg'];
        $image = $request->base64Img;
        $result = preg_match('/image\/(.*);/', $image, $matches);
        if (!$result || !in_array($matches[1], $allowableTypes)) {
            return 'not allowable extension';
        }
        $roomId = $request->roomId;
        $image = preg_replace('/data:image\/(.*);base64,/sx', '', $image);
        $imageName = uniqid().".$matches[1]";
        $relativeDirPath = "rooms/$roomId/images/specs";
        $fullDirpath = public_path()."/".$relativeDirPath;
         if(!file_exists($fullDirpath)) {
             mkdir("$fullDirpath",0777, true);
         }
        $imageRelativePath = 'images/' . $imageName;
        \File::put("$fullDirpath/$imageName", base64_decode($image));

        $spec->photo = "$relativeDirPath/$imageName";
        $spec->room_id = $roomId;
        $spec->save();
        return $spec->load('category');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($roomId, $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$roomId, $id)
    {
        $spec = Spec::find($request->selectedSpecObj['id']);
        $this->authorize('update', $spec);
        $spec->title = $request->title;
        $spec->category_id = $request->selectedSpecObj['category_id'];

        $allowableTypes = ['png', 'jpg', 'jpeg'];
        $image = $request->base64Img;
        $result = preg_match('/image\/(.*);/', $image, $matches);
        if (!$spec->photo && (!$result || !in_array($matches[1], $allowableTypes))) {
            return 'not allowable extension';
        }
        // DELETING OLD IMAGE, BEFORE SET NEW
        if($spec->photo && $image) {
            if (file_exists($filepath = public_path() . '/' . $spec->photo)) {
                unlink($filepath);
            }
        }
        if ($image) {
            $image = preg_replace('/data:image\/(.*);base64,/sx', '', $image);
            $imageName = uniqid().".$matches[1]";
            $imageRelativePath = 'images/' . $imageName;
            \File::put(public_path().'/'.$imageRelativePath, base64_decode($image));
            $spec->photo = $imageRelativePath;
        }

        $spec->save();
        return $spec->load('category');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $spec = Spec::find($id);

        $this->authorize('delete', $spec);
    }
}
