<?php

namespace App\Http\Controllers;

use App\Currency;
use App\Group;
use App\Order;
use App\Product;
use App\Spec;
use App\Status;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        return Product::where('room_id', $request->roomId)->with('currency', 'order', 'spec.category', 'users','groups')->get();
    }

    public function freeProducts(Request $request)
    {
        return Product::where('room_id', $request->roomId)->with('spec','order')->get()->filter(function ($product) {
            return !$product->groups->count();
        })->toJson();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->authorize('create', Product::class);

        $product = (new Product())->fill($request->all());
        if (Product::where('sn', $product->sn)->get()->count()) {
            return ['errors' => ['sn already exists']];
        }
        if (isset($request->spec['id'])) {
            $spec = Spec::find($request->spec['id']);
        } else {
            $spec = Spec::where('title', $request->spec)->where('room_id', $request->roomId)->first();
        }

        if (isset($request->status['id'])) {
            $status = Status::find($request->status['id']);
        } else {
            $status = Status::where('status', $request->status)->first();
        }

        if (isset($request->currency['id'])) {
            $currency = Currency::find($request->currency['id']);
        } else {
            $currency = Currency::where('symbol', $request->currency)->first();
        }

        if (isset($request->group['id'])) {
            $group = Group::find($request->group['id']);
        }

        if (isset($request->order['id'])) {
            $order = Order::find($request->order['id']);
        } else {
//            $order = Order::where('symbol', $request->currency)->first();
        }
        $product->is_guarantee = $request->isGuaranteeExisting ?: 0;
        $product->room_id = $request->roomId;
        $product->is_new = $request->isNewExisting ?? false;
        $product->guarantee_start = $request->guarantee['start'] ? preg_replace('/T.*/sx','',$request->guarantee['start']) : null;
        $product->guarantee_stop = $request->guarantee['start'] ?preg_replace('/T.*/sx','',$request->guarantee['end']) : null;
            $product->currency()->associate($currency);
            $product->spec()->associate($spec);
            $product->status()->associate($status);
            if(isset($order)) {
                $product->order()->associate($order);
                $product->save();

            } elseif (isset($group)) {
                $product->save();
                $product->groups()->attach($group);
            }
        return $product;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($roomId, $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $roomId, Product $product)
    {
        $this->authorize('update', $product);

        $product->fill($request->all());

        if (isset($request->spec['id'])) {
            $spec = Spec::find($request->spec['id']);
        } else {
            $spec = Spec::where('title', $request->spec)->where('room_id', $request->roomId)->first();
        }
        if (isset($request->status['id'])) {
            $status = Status::find($request->status['id']);
        } else {
            $status = Status::where('status', $request->status)->first();
        }
        if (isset($request->currency['id'])) {
            $currency = Currency::find($request->currency['id']);
        } else {
            $currency = Currency::where('symbol', $request->currency)->first();
        }
        if (isset($request->group['id'])) {
            $group = Group::find($request->group['id']);
        }
        if (isset($request->order['id'])) {
            $order = Order::find($request->order['id']);
        } else {
//            $order = Order::where('symbol', $request->currency)->first();
        }
        $product->is_guarantee = $request->isGuaranteeExisting ?: 0;
        $product->is_new = $request->isNewExisting ?? false;
        $product->guarantee_start = $request->guarantee['start'] ? preg_replace('/T.*/sx','',$request->guarantee['start']) : null;
        $product->guarantee_stop = $request->guarantee['start'] ?preg_replace('/T.*/sx','',$request->guarantee['end']) : null;
        $product->room_id = $request->roomId;
        $product->currency()->associate($currency);
        $product->spec()->associate($spec);
        $product->status()->associate($status);
        if(isset($order)) {
            $product->order()->associate($order);
            $product->save();

        } elseif (isset($group)) {
            $product->save();
            $product->groups()->attach($group);
        }
        $product->save();
        return $product;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($roomId, $productId)
    {

        $product = Product::find($productId);
        $this->authorize('delete', $product);

        $product->delete();
        return  ['message' => 'success'];
    }


}
