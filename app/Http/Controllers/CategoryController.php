<?php

namespace App\Http\Controllers;

use App\Category;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        return Category::where('room_id', $request->roomId)->get()->map(function ($item) {
            return $item->name;
        });
    }

    public function getObjects(Request $request)
    {
        return Category::where('room_id', $request->roomId)->with(
            'specs.products.status',
            'specs.products.spec',
            'specs.products.order',
            'specs.products.groups',
            'specs.products.currency'
        )->get();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->authorize('create', Category::class);

        $validator = \Validator::make($request->all(), [
            'name' => 'required'
        ]);
        if ($validator->fails()) {
            return ['errors' => $validator->errors()];
        }
        $category = new Category();
        $category->name = $request->name;
        $category->parent_id = $request->parent_id;
        $category->room_id = $request->roomId;
        $category->save();
        return $category->name;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $roomId, $catId)
    {
        $category = Category::find($catId);

        $this->authorize('update', $category);

        if(!$category) {
            abort(404);
        }
        $category->fill($request->except('id', 'specs'));
        $category->save();
        return ['message' => 'success'];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $category = Category::find($id);

        $this->authorize('delete', $category);

    }
}
