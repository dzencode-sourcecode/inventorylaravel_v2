<?php

namespace App\Http\Controllers;

use App\Category;
use App\Group;
use App\Product;
use App\User;
use GuzzleHttp\Client;
use Illuminate\Http\Request;

class GroupController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $groups =  Group::where('room_id', $request->roomId)->with('users.products',
            'products.spec.category',
            'products.order',
            'products.currency',
            'products.status',
            'orders'
        )->get();
        $roomId = $request->roomId;
        if ($groups && $groups->has('users') && $groups->users->count()) {
            $groups->users = $groups->users->filter( function($user)  {
                if ( $user->products->count()) {
                    $user->products = $user->products->filter( function ($product)  {
                        global $roomId;
                        return $product->room_id == $roomId;
                    });
                }
            });
        }

        if($groups->count() && $groups->has('products') && $groups->products->count()) {
            $groups->products = $groups->products->filter(function ($product) {
                global $roomId;
                return $product->room_id == $roomId;
            });
        }


        return $groups;
    }

    public function getGroupUsers(Request $request, $roomId, Group $group)
    {
        $ids = $group->users->map(function ($user) {
            return $user->saas_id;
        });
        $client = new Client();
        $response = $client->post(config('saas.api.getUsers'), [
            \GuzzleHttp\RequestOptions::JSON => [
                'ids' => $ids,
                'token' => session('saas.user_token')

            ]
        ]);
        $saasUsers = json_decode($response->getBody());

        $users =  $group->users->load('groups','products');
        foreach ($users as $user) {
            foreach ($saasUsers as $saasUser) {
                if($user->saas_id == $saasUser->id) {
                    $user->saasUser = $saasUser;
                }
            }
        }

        return $users;

    }

    public function addUser(Request $request, $roomId, Group $group, User $user)
    {
        $this->authorize('update', $group);

        $group->users()->attach($user);
        return ['message' => 'succes'];
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $this->authorize('create', Group::class);

        $group = new Group();

        if(!$request->image || $request->photo) {
            return 'no image loaded ';
        }

        $allowableTypes = ['png', 'jpg', 'jpeg'];
        $image = $request->image ?? $request->photo;
        $result = preg_match('/image\/(.*);/', $image, $matches);
        if (!$result || !in_array($matches[1], $allowableTypes)) {
            return 'not allowable extension';
        }
        $roomId = $request->roomId;

        $image = preg_replace('/data:image\/(.*);base64,/sx', '', $image);
        $imageName = uniqid().".$matches[1]";
        $relativeDirPath = "rooms/$roomId/images/groups";
        $fullDirpath = public_path()."/".$relativeDirPath;
        if(!file_exists($fullDirpath)) {
            mkdir("$fullDirpath",0755, true);
        }

        \File::put("$fullDirpath/$imageName", base64_decode($image));
        $group->title = $request->title;
        $group->photo = "$relativeDirPath/$imageName";
        $group->room_id = $roomId;
        $group->load('users', 'products.spec','products.currency','products.order', 'products.currency', 'orders');
        $group->save();
        //for client purposes
        $group->date = date('m-d-Y h:i:s', time());

        return $group;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($roomId, $id)
    {
        return Group::find($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $group = Group::find($id);
        $this->authorize('delete', $group);

        $group->delete();
        return ['message' => 'success'];
    }

    public function addProduct(Request $request,$roomId, Group $group)
    {
        $this->authorize('update', $group);

        $product = Product::find($request->product['id']);
        $product->groups()->attach($group);

        return $product->load('groups', 'spec.category', 'order', 'status', 'currency');
    }

    public function deleteProduct(Request $request,$roomId, Group $group, Product $product)
    {
        $this->authorize('update', $group);

        $group->products()->detach($product);

        return $product->load('groups', 'spec.category', 'order', 'status', 'currency');
    }
}
