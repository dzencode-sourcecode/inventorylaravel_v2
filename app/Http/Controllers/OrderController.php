<?php

namespace App\Http\Controllers;

use App\Order;
use Illuminate\Http\Request;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
//        return Order::all()->load(
//            'groups',
//            'users',
//            'products.currency',
//            'products.status',
//            'products.spec'
//            );

        return Order::where('room_id', $request->roomId)->with(
            'groups',
            'users',
            'products.currency',
            'products.status',
            'products.spec'
            )->get();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {


    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->authorize('create', Order::class);
//        $user = \App\Repository\UserRep::findFromSaasId(session('saas.user')['id']);
        $order = new Order();
        $order->fill($request->all());
        list($date) = explode('T', $order->date);
        $order->date = $date;
        $order->room_id = $request->roomId;
        $order->save();
        $order->load('groups', 'users', 'products.currency');
        return $order;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($roomId, $id)
    {
        return Order::find($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$roomId, $id)
    {
        $order = Order::find($id);

        $this->authorize('update', $order);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $order = Order::find($id);

        $this->authorize('delete', $order);

        $order->delete();
        return ['message' => 'success'];
    }
}
