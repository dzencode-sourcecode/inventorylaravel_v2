<?php

namespace App\Http\Controllers;

use App\Group;
use App\Product;
use App\User;
use function foo\func;
use Illuminate\Http\Request;
use GuzzleHttp\Client;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, $roomId)
    {
        $ids = User::select('saas_id')->get()->map(function ($item) {
            return $item->saas_id;
        });
        $client = new Client();
        $response = $client->post(config('saas.api.getUsers'), [
            \GuzzleHttp\RequestOptions::JSON => [
                'ids' => $ids,
                'token' => session('saas.user_token')
            ]
        ]);

        $roomId = $request->roomId;
        $saasUsers = json_decode($response->getBody());
        $users =  User::all()->load(
            'groups.users',
            'groups.products',
            'products.spec',
            'products.currency',
            'products.status',
            'products.order',
            'orders');
//        $users = User::with(['groups' => function($q) use ($roomId) {
//            $q->where('room_id', $roomId);
//        }])->get();
        foreach ($users as $user) {
            foreach ($saasUsers as $saasUser) {
                if($user->saas_id == $saasUser->id) {
                    $user->saasUser = $saasUser;
                }
            }
        }



//        $users = $users->filter(function ($user) use($roomId) {
//                $flag = false;
//                foreach ($user->saasUser->user_service_rooms as $usr) {
//                    if($usr->service_room_id == $roomId) $flag = true;
//                }
//                return $flag;
//        });
//        $users = $users->map( function( $user) use ($roomId) {
//            $user->groups = $user->groups()->where('room_id', $roomId)->with('users','products')->get();
//
//            $user->groups = [];
//            if ($user->has('products') && $user->products->count()) {
//                $user->set = $user->products->filter(function ($product) {
//                    global $roomId;
//                    return $product->room_id == $roomId;
//                });
//            }
//            if ($user->has('orders') && $user->orders->count()) {
//                $user->orders = $user->orders->filter(function ($order) {
//                    global $roomId;
//                    return $order->room_id == $roomId;
//                });
//            }
//            return $user;
//        });
//        dd($users);

        return $users;
    }

    public function addProductToUser(Request $request,$roomId, User $user, Product $product)
    {
        $this->authorize('update', $user);

        $user->products()->attach($product);
    }

    public function getUser(Request $request )
    {
        return session('saas.user');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$roomId, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($roomId, $id)
    {
        $user = User::find($id);
        $this->authorize('update', $user);
        $user->delete();
        return ['message' => 'success'];
    }

    public function deleteProduct(Request $request, $roomId, User $user, Product $product)
    {
        $this->authorize('update', $user);
        $user->products()->detach($product);
        return ['message' => 'success'];
    }

    public function deleteGroup(Request $request, $roomId, User $user, Group $group)
    {
        $this->authorize('update', $user);
        $user->groups()->detach($group);
        return ['message' => 'success'];
    }
}
