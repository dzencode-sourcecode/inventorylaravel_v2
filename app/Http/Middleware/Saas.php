<?php

namespace App\Http\Middleware;

use Closure;
use GuzzleHttp\Client;
use App\User;
use Auth;

class Saas
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $saasAuthUrl = (config("saas.auth") ?? "http://saas.dzencode.com/api/auth") ."?redirectTo=".route('index', $request->roomId);
        // if not auth
        if ((!session()->has('saas.user_token') || !session()->has('saas.user')) && !$request->query('token')) {
            return redirect($saasAuthUrl);
            // if requested back from sass with token
        } elseif ((!session()->has('saas.user_token') || !session()->has('saas.user')) && ($token = $request->query('token'))) {
            session()->put('saas.user_token',$token);
            $client = new Client();
            $url = config('saas.api.getAuthUser')."?token=$token";
            $response = $client->get($url);
            $user = json_decode($response->getBody(), true);
            if (!$user || isset($user['error'])) {
                return redirect($saasAuthUrl);
            };

            // create  user entry
            $inventoryUser = User::where('saas_id', $user['id'])->first();
            if(!$inventoryUser) {
                $inventoryUser = new User();
                $inventoryUser->saas_id = $user['id'];
                $inventoryUser->save();
            }
            Auth::login($inventoryUser, true);
            session()->put('saas.user', $user);
            session()->save();
            // if auth
            return $next($request);

        } elseif(session()->has('saas.user')) {
            if(!auth()->user() && ($user =  session('saas.user')) ) {
                $user = User::where('saas_id', $user['id'])->first();
                Auth::login($user, true);

            }
            return $next($request);

        }
        die('permission denied');
    }
}
