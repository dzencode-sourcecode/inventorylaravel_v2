<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Auth;
/**
 * @property int $id
 * @property string $timestamp
 * @property int $saas_id
 * @property Group[] $groups
 * @property Order[] $orders
 * @property Product[] $products
 */


class User extends Authenticatable
{

    public $timestamps = false;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'user';

    /**
     * @var array
     */
    protected $fillable = ['timestamp', 'saas_id'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function groups()
    {
        return $this->belongsToMany('App\Group');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function orders()
    {
        return $this->belongsToMany('App\Order', 'order_has_user');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function products()
    {
        return $this->belongsToMany('App\Product', 'user_has_item', null, 'item_id');
    }

    public function getRoles()
    {
        $roomId = request()->roomId;
        if(!$roomId) return [];
        $user = session('saas.user');
        $room = array_filter($user['user_service_rooms'], function ($usr) use ($roomId) {
            return $usr['service_room_id']== $roomId;
        })[0];
        return $room['roles'];
    }

    public function getStrRoles()
    {
        return array_map($this->getRoles(), function ($role) {
            return $role['name'];
        });
    }

    public function hasRole($str)
    {
        return in_array($str, $this->getStrRoles());
    }
}
