<?php


namespace App\Repository;


use App\User;

class UserRep
{
    public static function findFromSaasId($id)
    {
        return User::where('saas_id', $id)->first();
    }
}
